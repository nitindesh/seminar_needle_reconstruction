/*
Reference based on webpages and documents:
1. http://mathworld.wolfram.com/Ellipse.html
2. Direct Least Squares Fitting of Ellipses, A.W. Fitzgibbon et al.
3. RANSAC based three points algorithm for ellipse fitting of spherical object’s projection, S. Xu.
4. Guide to 3D Vision Computation Geometric Analysis and Implementation, K. Kanatani, Y. Sugaya, Y. Kanazawa
*/

#include <opencv2/core/types_c.h>
#include <iostream>

#define PI 3.14

// to encode the ellipse information
typedef struct{
    double x,y; // any given point
    double major_axis,minor_axis,phi_angle; // semi major axis, semi minor axis, tilt angle
    double focus1_x,focus1_y; // focus focus1 of the ellipse
    double focus2_x,focus2_y; // focus focus2 of the ellipse
} RFit;

//***************************************************************************************************************
// get the parameters of the ellipse
void getEllipseParameters ( double a, double b, double c, double d, double f, double g, RFit& ellipse)
{
    // find the center of the ellipse 
    ellipse.x = (c * d - b * f)/(b * b - a * c);
    ellipse.y = (a * f - b * d)/(b * b - a * c);
 
    // lengths - semi-axes 
    ellipse.major_axis = sqrt( (2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g))/((b*b-a*c)*(sqrt((a-c)*(a-c)+4*b*b)-(a+c))));
    ellipse.minor_axis = sqrt( (2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g))/((b*b-a*c)*(sqrt((a-c)*(a-c)+4*b*b)+(a+c))));
 
    // tilt - anti-clockwise
    ellipse.phi_angle = 0;

    // discretize and assign the tilt values
    if(b == 0 && a < c)
    {
	    ellipse.phi_angle = 0;
    }

    else if(b == 0 && a > c)
    {
   	    ellipse.phi_angle = 90;
    }

    else if(b != 0 && a < c)
    {
	    ellipse.phi_angle = 0.5 * arc_cotan_double( (a-c)/(2*b) );
    }

    else if(b != 0 && a > c)
    {
	    ellipse.phi_angle = 90 + 0.5 * arc_cotan_double( (a-c)/(2*b) );
    }

    if(ellipse.minor_axis > ellipse.major_axis)
    {
	    double ma_t = ellipse.major_axis;
	    ellipse.major_axis = ellipse.minor_axis;
	    ellipse.minor_axis = ma_t;
	    ellipse.phi_angle+=90;
    }
 
    double sq_residue; 
    if ( ellipse.major_axis > ellipse.minor_axis)
        sq_residue = sqrt ( ellipse.major_axis * ellipse.major_axis - ellipse.minor_axis * ellipse.minor_axis);
    else
        sq_residue = sqrt ( ellipse.minor_axis * ellipse.minor_axis - ellipse.major_axis * ellipse.major_axis);
  
    ellipse.focus1_x = ellipse.x - sq_residue * cos ( ellipse.phi_angle*PI/180);
    ellipse.focus1_y = ellipse.y - sq_residue * sin ( ellipse.phi_angle*PI/180);
    ellipse.focus2_x = ellipse.x + sq_residue * cos ( ellipse.phi_angle*PI/180);
    ellipse.focus2_y = ellipse.y + sq_residue * sin ( ellipse.phi_angle*PI/180);
}

//***************************************************************************************************************
// function to compute inverse cotan
double arc_cotan_double (double val)
{
    double acot = atan(1/val);
    return acot*180/PI;
}

//***************************************************************************************************************
// functiion to check if the point resides in the ellipse
bool isPointInEllipse (CvPoint point, RFit ellipse)
{
    double dist1 = sqrt ((point.x - ellipse.focus1_x) * (point.x - ellipse.focus1_x) + (point.y - ellipse.focus1_y) * (point.y - ellipse.focus1_y));
    double dist2 = sqrt ((point.x - ellipse.focus2_x) * (point.x - ellipse.focus2_x) + (point.y - ellipse.focus2_y) * (point.y - ellipse.focus2_y));

    double max_axis;
    if ( ellipse.major_axis > ellipse.minor_axis)
        max_axis = ellipse.major_axis;
    else
        max_axis = ellipse.minor_axis;

    if ( dist1+dist2 <= 2*max_axis)
        return true;
    else
        return false;
}

//***************************************************************************************************************
// function to use the parameter to get ellipse information
// update_cntr - count the number of points reside in the ellipse
RFit ellipseFit ( vector<CvPoint> p_points, int &update_cntr)
{
    RFit ellipse;
    update_cntr = 0;

    int i_idx [5];
    bool is_a_match = false;

    for ( int i=0; i<5; i++)
    {
        do 
	    {
        is_a_match = false;
        i_idx[i]=rand()%p_points.size();

            for(int j=0;j<i;j++)
	        {
		        if(i_idx[i] == i_idx[j])
		        {
			        is_a_match=true;
		        }
            }
        }
        while(is_a_match);
    }

    // in plain words, construct matrix A, such that A = D*U*V and further decomposable using SVD
    double concat_data [] = {
    p_points[i_idx[0]].x * p_points[i_idx[0]].x, 2 * p_points[i_idx[0]].x * p_points[i_idx[0]].y, p_points[i_idx[0]].
    y * p_points[i_idx[0]].y, 2 * p_points[i_idx[0]].x, 2 * p_points[i_idx[0]].y,
 
    p_points[i_idx[1]].x * p_points[i_idx[1]].x, 2 * p_points[i_idx[1]].x * p_points[i_idx[1]].y, p_points[i_idx[1]].
    y * p_points[i_idx[1]].y, 2 * p_points[i_idx[1]].x, 2 * p_points[i_idx[1]].y,
 
    p_points[i_idx[2]].x * p_points[i_idx[2]].x, 2 * p_points[i_idx[2]].x * p_points[i_idx[2]].y, p_points[i_idx[2]].
    y * p_points[i_idx[2]].y, 2 * p_points[i_idx[2]].x, 2 * p_points[i_idx[2]].y,
 
    p_points[i_idx[3]].x * p_points[i_idx[3]].x, 2 * p_points[i_idx[3]].x * p_points[i_idx[3]].y, p_points[i_idx[3]].
    y * p_points[i_idx[3]].y, 2 * p_points[i_idx[3]].x, 2 * p_points[i_idx[3]].y,
 
    p_points[i_idx[4]].x * p_points[i_idx[4]].x, 2 * p_points[i_idx[4]].x * p_points[i_idx[4]].y, p_points[i_idx[4]].
    y * p_points[i_idx[4]].y, 2 * p_points[i_idx[4]].x, 2 * p_points[i_idx[4]].y }; // ends here

  
    CvMat helper_matrix = cvMat (5, 5, CV_64F, concat_data);
  
    CvMat *D,*U,*V;
  
    // for SVD
    // mxm unitary matrix
    D = cvCreateMat ( 5, 5, CV_64F);
    // doagonal matrix
    U = cvCreateMat ( 5, 5, CV_64F);
    // nxn unitary matrix
    // vector V is the coefficient a,b,c,d,f,g
    V = cvCreateMat ( 5, 5, CV_64F);
    
    // Singular Value Decomposition
    cvSVD (&helper_matrix, D, U, V, CV_SVD_MODIFY_A);
 
    // params of/for the ellipse
    double a, b, c, d, f, g;

    // utilise V to get ellipse parameters
    a=cvmGet ( V, 0, 4);
    b=cvmGet ( V, 1, 4);
    c=cvmGet ( V, 2, 4);
    d=cvmGet ( V, 3, 4);
    f=cvmGet ( V, 4, 4);
    g=1;
 
    getEllipseParameters ( a, b, c, d, f, g, ellipse);
 
    vector<CvPoint>::iterator point_iterator;
  
    if ( ellipse.major_axis > 0 && ellipse.minor_axis > 0)
    {
	    for ( point_iterator = p_points.begin(); point_iterator != p_points.end(); point_iterator++)
	    {
            CvPoint point = *point_iterator; 
	        if ( isPointInEllipse ( point,ellipse))
            {
		        update_cntr++;
            }
        }
    }
    // return the rfit struct and its members 
    return ellipse;
}
