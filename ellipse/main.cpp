#include <iostream>

#include <Eigen/Dense>

struct line {
	line(float a, float b) : a(a), b(b) {}
	float apply(float x) { return a*x + b; }

	const float a, b;
};

std::pair<line, line> fitLines(Eigen::MatrixXf& A, int dim)
{
	if (A.cols() < dim + 1)
		throw std::runtime_error("not enough unknowns");
	if (A.rows() < dim)
		throw std::runtime_error("not enough equations");

	int m = std::min(A.cols(), A.rows());
	Eigen::MatrixXf R = Eigen::HouseholderQR<Eigen::MatrixXf>(A).matrixQR().triangularView<Eigen::Upper>();
	auto svd = R.block(A.cols()-dim, A.cols()-dim, m-A.cols()+dim, dim).jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
	auto n = svd.matrixV().col(dim - 1);
	auto sol = R.block(0, 0, A.cols()-dim, A.cols()-dim).colPivHouseholderQr();
	auto c = -sol.solve(R.block(0, A.cols()-dim, A.cols()-dim, dim)) * n;

	line l1(n[0]/-n[1], c[0]/-n[1]);
	line l2(n[0]/-n[1], c[1]/-n[1]);

	return std::make_pair(l1, l2);
}

int main(int argc, char* argv[])
{
	int n, split;
	float x, y;

	std::cin >> n >> split;

	Eigen::MatrixXf A((n - split) * 2, 4);
	for (int i = 0; i < n; i++) {
		std::cin >> x >> y;
		if (i >= split) {
			A(i - split, 0) = 1;
			A(i - split, 1) = 0;
			A(i - split, 2) = x;
			A(i - split, 3) = y;
		}
	}
	for (int i = n; i < 2*n; i++) {
		std::cin >> x >> y;
		if (i >= n + split) {
			A(i - 2*split, 0) = 0;
			A(i - 2*split, 1) = 1;
			A(i - 2*split, 2) = x;
			A(i - 2*split, 3) = y;
		}
	}

	auto res = fitLines(A, 2);
	auto l1 = res.first;
	auto l2 = res.second;

	std::cout << l1.apply(1.27969) << " " << l1.apply(2.03125) << std::endl;
	std::cout << l2.apply(1.27969) << " " << l2.apply(2.03125) << std::endl;

	return 0;
}
