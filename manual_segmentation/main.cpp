/*-------------------------------------------------------------------*/
/*                                                                   */
/*                    Zeiss Interventional Imaging                   */
/*                         Research Solution                         */
/*                   -----------------------------                   */
/*  Chair for Computer Aided Medical Procedures & Augmented Reality  */
/*                  Technische Universit�t M�nchen                   */
/*                                                                   */
/*-------------------------------------------------------------------*/

#include <fstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <string>
#include <opencv2/opencv.hpp>
#include "OCTDefs.hpp"

#include <pcl/common/common_headers.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

#include <thread>
#include <chrono>
#include <regex>
#include <boost/filesystem.hpp>

boost::shared_ptr<pcl::visualization::PCLVisualizer> simpleVis(pcl::PointCloud<pcl::PointXYZI>::ConstPtr cloud, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> rgb);
void MatToPoinXYZ(cv::Mat& OpencVPointCloud, cv::Mat& labelInfo, int z, pcl::PointCloud<pcl::PointXYZI>::Ptr& point_cloud_ptr, int height, int width);
void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* viewer_void);


pcl::PointCloud<pcl::PointXYZI>::Ptr point_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>);
pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> rgb(point_cloud_ptr, 192, 192, 192);

bool waitKey = false;
int volumeBScans = 128;

static std::vector<std::string> getImagePaths(const std::string& path)
{
	std::regex fname("^\\d{3}\\.bmp$");
	std::vector<std::string> paths;

	for (boost::filesystem::directory_iterator it(path); it != boost::filesystem::directory_iterator(); ++it)
		if (boost::filesystem::is_regular_file(it->status()) && std::regex_match(it->path().filename().string(), fname))
			paths.push_back(it->path().string());

	std::sort(paths.begin(), paths.end());
	return paths;
}

int main(int argc, char** argv)
{
	int countIm = 0;
	std::string keys =
		"{help h ?    |      | Display the program help.}"
		"{@input      |      | Path to the directory containing recorded OCT frames.}"
		;
	cv::CommandLineParser parser(argc, argv, keys);
	parser.about("Zeiss Interventional Imaging Research Solution");
	if (parser.has("help"))
	{
		parser.printMessage();
		exit(0);
	}
	std::string inputDirectory = parser.get<std::string>(0);

	if (!parser.check())
	{
		parser.printErrors();
		exit(0);
	}

	//get the path to the images
	std::replace(inputDirectory.begin(), inputDirectory.end(), '\\', '/');
	int lastSlashIndex = inputDirectory.find_last_of('/', inputDirectory.size());
	if (lastSlashIndex < (int)inputDirectory.size() - 1)
		inputDirectory += "/";

	int number = 0;

	//	go through all frames
	for (const auto& path : getImagePaths(inputDirectory))
	{
		//read the image in grayscale
		cv::Mat imageGray = cv::imread(path, CV_LOAD_IMAGE_GRAYSCALE);

		//flip and transpose the image
		cv::Mat transposedOCTimage;
		cv::flip(imageGray, imageGray, 0);
		cv::transpose(imageGray, transposedOCTimage);

		//set a threshold (0.26)
		cv::Mat thresholdedImage;
		cv::threshold(transposedOCTimage, thresholdedImage, 0.26 * 255, 1, 0);

		//use a median blur filter
		cv::Mat filteredImage;
		cv::medianBlur(thresholdedImage, filteredImage, 3);

		//label the image
		cv::Mat labelledImage;
		cv::Mat labelStats;
		cv::Mat labelCentroids;
		int numLabels = cv::connectedComponentsWithStats(thresholdedImage, labelledImage, labelStats, labelCentroids);

		//for every label with more than 400 points process it further for adding points to the cloud
		for (int i = 1; i < numLabels; i++) {
			if (labelStats.at<int>(i, cv::CC_STAT_AREA) > 400) {
				cv::Mat labelInfo = labelStats.row(i);
				MatToPoinXYZ(filteredImage, labelInfo, number, point_cloud_ptr, thresholdedImage.rows, thresholdedImage.cols);
			}
		}
		number++;

		//show the images
		thresholdedImage = thresholdedImage*255;
		filteredImage = filteredImage*255;
		cv::imshow("OCT", filteredImage);

		cv::waitKey(10);
		countIm = countIm + 1;

	}
	// Cluster segmentation
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::copyPointCloud(point_cloud_ptr, cloud_filtered);
	cloud_filtered->points.resize(point_cloud_ptr->size());
	for (size_t i = 0; i < point_cloud_ptr->points.size(); i++) {
		cloud_filtered->points[i].x = point_cloud_ptr->points[i].x;
		cloud_filtered->points[i].y = point_cloud_ptr->points[i].y;
		cloud_filtered->points[i].z = point_cloud_ptr->points[i].z;
	}
	//
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (cloud_filtered);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance (0.1); //
	ec.setMinClusterSize (100);
	ec.setMaxClusterSize (25000);
	ec.setSearchMethod (tree);
	ec.setInputCloud (cloud_filtered);
	ec.extract (cluster_indices);

	std::vector<pcl::PointCloud<pcl::PointXYZ>> clusters;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
	{
		pcl::PointCloud<pcl::PointXYZ> cloud_cluster;
		for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
			cloud_cluster.points.push_back (cloud_filtered->points[*pit]); //*
		cloud_cluster.width = cloud_cluster.points.size ();
		cloud_cluster.height = 1;
		cloud_cluster.is_dense = true;
		std::cout << "PointCloud representing the Cluster: " << cloud_cluster.points.size () << " data points." << std::endl;
		clusters.push_back(cloud_cluster);
	}

	std::cout << clusters.size() << std::endl;
	pcl::PointCloud<pcl::PointXYZI> temp;
	pcl::copyPointCloud(clusters[1], temp);
	pcl::PointCloud<pcl::PointXYZI>::Ptr cluster_vis;
	cluster_vis = boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI>> (new pcl::PointCloud<pcl::PointXYZI>(temp));
	// open a viewer and show the cloud
	//boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = simpleVis(point_cloud_ptr, rgb);
	//viewer->updatePointCloud(point_cloud_ptr, rgb,  "sample cloud");
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = simpleVis(cluster_vis, rgb);
	viewer->updatePointCloud(cluster_vis, rgb,  "sample cloud");

	pcl::io::savePCDFileASCII ("segmentation.pcd", temp);

	while (!viewer->wasStopped())
	{
		viewer->spinOnce(100);
		std::this_thread::sleep_for(std::chrono::microseconds(100000));
	}
	return 0;
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> simpleVis(pcl::PointCloud<pcl::PointXYZI>::ConstPtr cloud, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> rgb)
{
	// --------------------------------------------
	// -----Open 3D viewer and add point cloud-----
	// --------------------------------------------
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
	viewer->setBackgroundColor(0, 0, 0); 
	viewer->addPointCloud<pcl::PointXYZI>(cloud, rgb, "sample cloud");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
	viewer->addCoordinateSystem(1.0);
	viewer->initCameraParameters();
	viewer->spinOnce();
	return (viewer);
}

void MatToPoinXYZ(cv::Mat& OpencVPointCloud, cv::Mat& labelInfo, int z, pcl::PointCloud<pcl::PointXYZI>::Ptr& point_cloud_ptr, int height, int width)
{
	//get the infos for the bounding box
	int x = labelInfo.at<int>(0, cv::CC_STAT_LEFT);
	int y = labelInfo.at<int>(0, cv::CC_STAT_TOP);
	int labelWidth = labelInfo.at<int>(0, cv::CC_STAT_WIDTH);
	int labelHeight = labelInfo.at<int>(0, cv::CC_STAT_HEIGHT);
	//go through points in bounding box
	for (int j = y; j < y+labelHeight; j++) {
		//indicate if first point with intensity = 1 in row has been found
		bool firstNotFound = true;
		//position of last point with intensity = 1 in row
		int lastPointPosition = 0;
		for (int i = x; i < x+labelWidth; i++)
		{
			if (OpencVPointCloud.at<uchar>(j,i) >= 1.0f){
				if (firstNotFound) {
					firstNotFound = false;
				}
				lastPointPosition = i;
			}
		}
		if (!firstNotFound) {
			//add the last point with intensity = 1 in row to the point cloud
			pcl::PointXYZI point;
			point.x = (float)z / volumeBScans * 2.6f;
			point.y = (float)j / height * 3.0f;
			point.z = (float)lastPointPosition / width * 2.0f;
			point.intensity = OpencVPointCloud.at<uchar>(j, lastPointPosition);
			point_cloud_ptr->points.push_back(point);
		}
	}
}
