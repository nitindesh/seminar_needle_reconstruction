#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>

#include <opencv2/opencv.hpp>
#include <boost/range/combine.hpp>
#include <thread>

inline cv::Point2f applyPoint(const cv::Vec4f& line, float x)
{
	const float m = line[1] / line[0];
	return cv::Point2f(x, m*(x - line[2]) + line[3]);
}

inline float distPointLine(const cv::Vec4f& line, const cv::Point2f& point)
{
	const float m = line[1] / line[0];
	return std::fabs(m*(point.x - line[2]) - point.y + line[3])
		/ std::sqrt(m*m + 1);
}

float wellFitting(const std::vector<cv::Point2f>& v)
{
	cv::Vec4f line;
	cv::fitLine(v, line, cv::DIST_L2, 0, 0.01, 0.01);

	float err = 0;
	for (const auto& i : v)
		err += std::fabs(distPointLine(line, i));
	return err;
}

int bestTwoLines(const std::vector<cv::Point2f>& v)
{
	float bestErr = std::numeric_limits<float>::max();
	int bestI;

	for (int i = 2; i < v.size() - 2; i++) {
		float err = wellFitting(std::vector<cv::Point2f>(v.begin(), v.begin() + i)) 
					+ wellFitting(std::vector<cv::Point2f>(v.begin() + i, v.end()));

		if (err < bestErr) {
			bestErr = err;
			bestI = i;
		}
	}

	return bestI;
}

int main(int argc, char* argv[])
{
	pcl::PointCloud<pcl::PointXYZI> cloud;
	pcl::io::loadPCDFile<pcl::PointXYZI>(argv[1], cloud);

	std::vector<cv::Point2f> points;
	// std::transform(cloud.begin(), cloud.end(), std::back_inserter(points), [](const auto& i){ return cv::Point(i.x, i.y); });
	for (const auto& i : cloud)
		points.push_back(cv::Point2f(i.x, i.y));

	std::map<float, float> lowPoints, highPoints;
	for (const auto& i : cloud) {
		lowPoints[i.x] = lowPoints.find(i.x) != lowPoints.end() ? std::min(i.y, lowPoints[i.x]) : i.y;
		highPoints[i.x] = highPoints.find(i.x) != highPoints.end() ? std::max(i.y, highPoints[i.x]) : i.y;
	}

	std::vector<cv::Point2f> diff;
	for (const auto& i : boost::combine(lowPoints, highPoints)) {
		std::pair<float, float> pa, pb;
		boost::tie(pa, pb) = i;
		diff.push_back(cv::Point2f(pa.first, std::fabs(pa.second - pb.second)));
	}

	cv::Vec4f line;
	cv::fitLine(diff, line, cv::DIST_L2, 0, 0.01, 0.01);
	int split = bestTwoLines(diff);

	std::cout << lowPoints.size() << " " << split << std::endl;
	for (const auto& i : lowPoints)
		std::cout << i.first << " " << i.second << std::endl;
	std::cout << std::endl;
	for (const auto& i : highPoints)
		std::cout << i.first << " " << i.second << std::endl;

	return 0;
}
