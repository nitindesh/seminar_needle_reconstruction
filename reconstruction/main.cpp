#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <iterator>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <pcl/io/vtk_lib_io.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <boost/math/constants/constants.hpp>
#include <opencv2/opencv.hpp>
#include <iterator>

int main(int argc, char* argv[])
{
	float b1 = 0.45f; //obtained from ellipse fitting
	Eigen::Vector3f b(0.27f,0.33f,0); //obtained from ellipse fitting (minor axes of ellipse)
	Eigen::Vector3f C_(1.25f,1.635f,0);//obtained from ellipse fitting
	Eigen::Vector3f E_l(0.25f,0.5f,0); //ojbtained from ellipse fitting (vector major axes ellipse)
	Eigen::Vector3f D_(1.5f,2.0f,0); //obtained from ellipse fitting	
	Eigen::Vector3f T_(1.0f,1.25f,0); //obtained from ellipse fitting

	float d; //Diameter
	float k0;  	
	float b0; 
	float k0_; 
	float b0_; 
	float theta; //tilt angle between X0Y Plane and vector n
	float phi;	//rotation angle	
	Eigen::Vector3f C;//Centerpoint, needs to be calculated.	
	Eigen::Vector3f n_xoy; //vector of mid line needle
	float Xi = 0.4;
	int quadrant = 0;
	const float pi = boost::math::constants::pi<float>();
	float beta; //beevel angle


	pcl::PointCloud<pcl::PointXYZI> cloud;
	pcl::io::loadPCDFile<pcl::PointXYZI>(argv[1], cloud);
	
	//reeding in points	
	std::vector<cv::Point3f> points;
	for (const auto& i : cloud)
		points.push_back(cv::Point3f(i.x, i.z, i.y));

	//get the peak points of each B-Scan image and stores the coordinates	
	std::map<float, cv::Vec2f> highPointsZ, lowPointsZ;
	for (const auto& i : cloud)
		if (highPointsZ.find(i.x) == highPointsZ.end() || highPointsZ[i.x][1] < i.z)
			highPointsZ[i.x] = cv::Vec2f(i.y, i.z);
	for (const auto& i : cloud)
		if (lowPointsZ.find(i.x) == lowPointsZ.end() || lowPointsZ[i.x][1] > i.z)
			lowPointsZ[i.x] = cv::Vec2f(i.y, i.z);
	
	//printing points
	typedef std::map<float, cv::Vec2f>::const_iterator MapIterator;
	for (MapIterator iter = highPointsZ.begin(); iter != highPointsZ.end(); iter++)
	{
    std::cout << "Key: " << iter->first << "Values:" <<iter->second[0]<<iter->second[1] << std::endl;    
	}

	std::vector<cv::Point3f> vectorHighPoints;
	std::transform(highPointsZ.begin(), highPointsZ.end(), std::back_inserter(vectorHighPoints), 
		[](const  std::pair<float, cv::Vec2f>& x){ return cv::Point3f(x.first, x.second[0], x.second[1]); }); 


	//3D linefitting to get k0, k0_, b0 and b0_
	cv::Vec6f line;
	cv::fitLine(vectorHighPoints, line, cv::DIST_L2, 0, 0.01, 0.01); //output line: (vx, vy, vz, x0, y0, z0), where (vx, vy, vz) is a normalized vector
		
 	//construct l1 and scale it to l1(1,k0,k0_)
	Eigen::Vector3f l1(line[0],line[1],line[2]); 	
	l1[2]=l1[2]/l1[0];	
	l1[1]=l1[1]/l1[0];	
	l1[0]=l1[0]/l1[0];
	std::cout<<"Line 1: " <<l1<<std::endl;

 	//arbitary point N on line	
	Eigen::Vector3f N(line[3],line[4],line[5]); 
	//scalar for N to get the point (0,x,y)
	float scalar = -N[0]/l1[0];
	N=N+scalar*l1;
	std::cout<<"Point N(0,b0,b0_)"<<N<<std::endl;

 	//get k0, b0, k0_ and b0_ via N = (0;b0;b0_) and l1= n = (1;k0;k0_)
	b0 = N[1];	
	k0 = l1[1];
	k0_ = l1[2];
	b0_ = N[2];

 	//Calculate theta via arctan and normalized vector (Angle beteween l1 and X0Y plane)
	theta = std::atan(l1[2]/std::sqrt(l1[0]*l1[0]+l1[1]*l1[1]));

 	//calculate diameter
	d = 2 * std::fabs(b1-b0)/(std::sqrt(1+(k0*k0)));
	std::cout<<"Diameter d "<< d << std::endl;
 	
	//get Centerpoint C
	C[0]=C_[0];
	C[1]=C_[1];
	C[2]=k0_*C_[0]+b0_-(d/2*std::cos(theta));	
	std::cout<<"Centerpoint C "<< C << std::endl;

 	//Get quadrant and phi with image slice closest to C. Calculates Bounding Box to get bz-cz 	
 		std::map<float, cv::Vec2f>::iterator it;
		std::map<float, cv::Vec2f>::iterator it2;
		cv::Point3f B;
 	
		//Use C[0] to get points of slice before and after C
		for(it = highPointsZ.begin();it!=highPointsZ.end();it++)
			if(it->first>C[0])
				break;
		float XAfter=it->first;	
		it--;
		float XBefore=it->first;
		
		if(std::fabs(XAfter-C[0])<std::fabs(C[0]-XBefore))
			it++;
	
	 	//Calculate Boundingbox	
		float hc = (it->second)[2]-lowPointsZ[it->first][2];
		float bz =(it->second)[2]-hc/2;
		n_xoy = l1;
		n_xoy[2]=0;
		
		//Calculate Quadrant
		auto cross = n_xoy.cross(E_l);
		if(cross[2]<=0 && (bz-C[2])>Xi*hc){
				quadrant = 2;
				phi = std::acos(2*b.norm()/d);}		
		else if(cross[2]<=0 && (bz-C[2])<=Xi*hc){
				quadrant = 1;
	 			phi = pi - std::acos(2*b.norm()/d);}		
	 	else if(cross[2]>=0 && (bz-C[2])<=Xi*hc){
	 			quadrant = 1;
	 			phi = pi + std::acos(2*b.norm()/d);}		
		else if(cross[2]>=0 && (bz-C[2])>Xi*hc){
	 			quadrant = 2;
	 			phi = 2*pi - std::acos(2*b.norm()/d);}		


	//reconstruct point D via D_T and DT. dz has two solutions. Calculate dz, with D_TDT =(Dx,Dy,Dz) = D_D*Rx(−α1)*Ry(α2) with D_D =(0,0,1). 
 	
 	//rotation of l1 around alpha1	
	float alpha_1 = std::atan(l1[1]/l1[0]);	
	std::cout<<"Rotation angle alpha1"<< alpha_1 << std::endl;	
	float radAlpha_1 = alpha_1*pi/180;
	auto rotation1 = Eigen::AngleAxisf(radAlpha_1 ,  Eigen::Vector3f::UnitZ()); 
	auto nYOZ = rotation1 * l1;
	
	//rotation of rotated l1 around alpha2
 	float alpha_2 = std::atan(l1[2]/l1[1]);
	alpha_2 = (90-alpha_2)*pi/180; 
	std::cout<<"Rotation angle alpha2"<< alpha_2 << std::endl;
	auto rotation2 = Eigen::AngleAxisf(radAlpha_1, Eigen::Vector3f::UnitX());

	//calculate dz and point D
 		auto D_T=rotation2 * (rotation1*(D_-C));
		std::cout<<"D_T "<< D_T << std::endl; 	
		Eigen::Vector3f D_D(0,0,1); //normalized distance vector between point D_ and D 
 		auto D_temp = rotation2*(rotation1*D_D);
		std::cout<<"D_temp: "<< D_temp << std::endl;
 		float dz;
		float rootD = std::sqrt(D_temp[2]*(d*d*(D_temp[0]*D_temp[0]+D_temp[1]*D_temp[1])-4*(D_temp[0]*D_T[1]-D_temp[1]*D_T[0])));
		std::cout<<"rootD first part: "<< d*d*(D_temp[0]*D_temp[0]+D_temp[1]*D_temp[1]) << std::endl;
		std::cout<<"rootD second part: "<< 4*(D_temp[0]*D_T[1]-D_temp[1]*D_T[0]) << std::endl;
		std::cout<<"rootD third part:"<< D_temp[2]*(d*d*(D_temp[0]*D_temp[0]+D_temp[1]*D_temp[1])-4*(D_temp[0]*D_T[1]-D_temp[1]*D_T[0])) << std::endl;	
		std::cout<<"rootD: "<< rootD << std::endl;
 		if(pi/2 <= phi && phi <3*pi/2){		
 			dz=(rootD-(2*D_T[2]*(D_temp[0]*D_temp[0]+D_temp[1]*D_temp[1])-2*D_temp[2]*(D_temp[0]*D_T[0]+D_temp[1]*D_T[0]))/(2*D_temp[0]*D_temp[0]+D_temp[1]*D_temp[1]));
			std::cout<<"dz in if: "<< dz << std::endl;}
 		else{
 			dz=(rootD+(2*D_T[2]*(D_temp[0]*D_temp[0]+D_temp[1]*D_temp[1])-2*D_temp[2]*(D_temp[0]*D_T[0]+D_temp[1]*D_T[0]))/(2*D_temp[0]*D_temp[0]+D_temp[1]*D_temp[1]));
			std::cout<<"dz in else: "<< dz << std::endl;}

 		Eigen::Vector3f D(D_[0], D_[1], dz);


 	//reconstruct point T via T_T and TT. tz has two solutions. Calculate tz, with T_TTT =(Tx,Ty,Tz) = T_T*Rx(−α1)*Ry(α2) with T_T =(0,0,1). 
 
 		auto T_T=rotation2 * (rotation1*(T_-C));

 		//calculatind tz and point T
		Eigen::Vector3f t_t(0,0,1); //normalized distance vector between point T_ and T
 		auto T_temp = rotation2*(rotation1*t_t);
 		float tz;
		float rootT = std::sqrt(T_temp[2]*(d*d*(T_temp[0]*T_temp[0]+T_temp[1]*T_temp[1])-4*(T_temp[0]*T_T[1]-T_temp[1]*T_T[0])));
	 	if(pi/2 <= phi && phi <3*pi/2)		
	 		tz=(rootT-(2*T_T[2]*(T_temp[0]*T_temp[0]+T_temp[1]*T_temp[1])-2*T_temp[2]*(T_temp[0]*T_T[0]+T_temp[1]*T_T[0]))/(2*T_temp[0]*T_temp[0]+T_temp[1]*T_temp[1]));	
	 	else
	 		tz=(rootT+(2*T_T[2]*(T_temp[0]*T_temp[0]+T_temp[1]*T_temp[1])-2*T_temp[2]*(T_temp[0]*T_T[0]+T_temp[1]*T_T[0]))/(2*T_temp[0]*T_temp[0]+T_temp[1]*T_temp[1]));
	 	
		Eigen::Vector3f T(T_[0], T_[1], tz);

	//calculate beta for varification
	Eigen::Vector3f distVec = C-D ;
	distVec.norm();
	beta=std::acos(d/(2*distVec.norm()));
	std::cout<<"Needle beevel angle beta"<< beta << std::endl;
	std::cout<<"Coordinates of Point D"<< D << std::endl;
	std::cout<<"Coordinates of Point T"<< T << std::endl;
	std::cout<<"Half Major axes CD of original Ellipse"<< distVec << std::endl;
return 0;
}
