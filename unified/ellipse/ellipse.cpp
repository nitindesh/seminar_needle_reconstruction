#include <ellipse.h>

#include <Eigen/Dense>
#include <pcl/visualization/pcl_plotter.h>
#include <vector>

struct line {
	line(float a, float b) : a(a), b(b) {}
	float apply(float x) { return a*x + b; }

	const float a, b;
};

static std::pair<line, line> fitLines(Eigen::MatrixXf& A, int dim)
{
	if (A.cols() < dim + 1)
		throw std::runtime_error("not enough unknowns");
	if (A.rows() < dim)
		throw std::runtime_error("not enough equations");

	int m = std::min(A.cols(), A.rows());
	Eigen::MatrixXf R = Eigen::HouseholderQR<Eigen::MatrixXf>(A).matrixQR().triangularView<Eigen::Upper>();
	auto svd = R.block(A.cols()-dim, A.cols()-dim, m-A.cols()+dim, dim).jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
	auto n = svd.matrixV().col(dim - 1);
	auto sol = R.block(0, 0, A.cols()-dim, A.cols()-dim).colPivHouseholderQr();
	auto c = -sol.solve(R.block(0, A.cols()-dim, A.cols()-dim, dim)) * n;

	line l1(n[0]/-n[1], c[0]/-n[1]);
	line l2(n[0]/-n[1], c[1]/-n[1]);

	return std::make_pair(l1, l2);
}

std::pair<line, line> fitParallelLines(std::tuple<std::map<float, float>, std::map<float, float>, int>& data, bool vis)
{
	pcl::visualization::PCLPlotter plotter("plotter");

	auto lowPoints = std::get<0>(data);
	auto highPoints = std::get<1>(data);
	auto split = std::get<2>(data);

	std::vector<std::pair<double, double>> points;
	std::transform(lowPoints.begin(), lowPoints.end(), std::back_inserter(points), [](const auto& i){ return std::make_pair(i.first, i.second); });
	std::transform(highPoints.begin(), highPoints.end(), std::back_inserter(points), [](const auto& i){ return std::make_pair(i.first, i.second); });

	std::vector<std::pair<double, double>> tipPoints, bodyPoints;
	tipPoints.insert(tipPoints.end(), points.begin(), points.begin() + split);
	tipPoints.insert(tipPoints.end(), points.begin() + points.size() / 2, points.begin() + points.size() / 2 + split);
	bodyPoints.insert(bodyPoints.end(), points.begin() + split, points.begin() + points.size() / 2);
	bodyPoints.insert(bodyPoints.end(), points.begin() + points.size() / 2 + split, points.end());

	if (vis) {
		plotter.addPlotData(tipPoints, "Tip points", vtkChart::POINTS);
		plotter.addPlotData(bodyPoints, "Body points", vtkChart::POINTS);
	}

	Eigen::MatrixXf A(bodyPoints.size(), 4);
	for (unsigned int i = 0; i < bodyPoints.size(); i++) {
		A(i, 0) = i < bodyPoints.size() / 2 ? 1 : 0;
		A(i, 1) = i < bodyPoints.size() / 2 ? 0 : 1;
		A(i, 2) = bodyPoints[i].first;
		A(i, 3) = bodyPoints[i].second;
	}

	auto res = fitLines(A, 2);
	auto l1 = res.first;
	auto l2 = res.second;

	if (vis) {
		points.clear();
		points.push_back(std::make_pair(bodyPoints[0].first, l1.apply(bodyPoints[0].first)));
		points.push_back(std::make_pair(bodyPoints[bodyPoints.size() / 2 - 1].first, l1.apply(bodyPoints[bodyPoints.size() / 2 - 1].first)));
		plotter.addPlotData(points, "Line 1");

		points.clear();
		points.push_back(std::make_pair(bodyPoints[bodyPoints.size() / 2].first, l2.apply(bodyPoints[bodyPoints.size() / 2].first)));
		points.push_back(std::make_pair(bodyPoints[bodyPoints.size() - 1].first, l2.apply(bodyPoints[bodyPoints.size() - 1].first)));
		plotter.addPlotData(points, "Line 2");
		plotter.plot();
	}

	return res;
}

void ellipse(std::tuple<std::map<float, float>, std::map<float, float>, int>& data, bool vis)
{
	auto res = fitParallelLines(data, vis);
}
