#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <map>

void ellipse(std::tuple<std::map<float, float>, std::map<float, float>, int>& data, bool vis = false);

#endif
