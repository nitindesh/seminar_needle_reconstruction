#include <segmentation.h>
#include <projection.h>
#include <ellipse.h>

#include <opencv2/opencv.hpp>

int main(int argc, char** argv)
{
	std::string keys =
		"{help h ?    |      | Display the program help.}"
		"{@input      |      | Path to the directory containing recorded OCT frames.}"
		"{segmentation s ? | | Stop at segmentation step. }"
		"{projection p ?   | | Stop at projection step. }"
		"{ellipse e ? 	   | | Stop at ellipse step. }"
		;
	cv::CommandLineParser parser(argc, argv, keys);
	parser.about("Zeiss Interventional Imaging Research Solution");
	if (parser.has("help"))
	{
		parser.printMessage();
		exit(0);
	}
	std::string inputDirectory = parser.get<std::string>(0);

	if (!parser.check())
	{
		parser.printErrors();
		exit(0);
	}

	auto needle_pcl = segment(inputDirectory);
	if (parser.has("segmentation")) {
		segVisualize(needle_pcl);
		return 0;
	}

	bool vis = parser.has("projection");
	auto proj = projection(needle_pcl, vis);
	if (vis)
		return 0;

	vis = parser.has("ellipse");
	ellipse(proj, vis);
	if (vis)
		return 0;

	return 0;
}
