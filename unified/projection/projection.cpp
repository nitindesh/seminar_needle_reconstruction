#include <projection.h>

#include <opencv2/opencv.hpp>
#include <boost/range/combine.hpp>
#include <thread>

#include <opencv2/highgui.hpp>
#include <opencv2/plot.hpp>

#include <pcl/visualization/pcl_plotter.h>

static inline cv::Point2f applyPoint(const cv::Vec4f& line, float x)
{
	const float m = line[1] / line[0];
	return cv::Point2f(x, m*(x - line[2]) + line[3]);
}

static inline float distPointLine(const cv::Vec4f& line, const cv::Point2f& point)
{
	const float m = line[1] / line[0];
	return std::fabs(m*(point.x - line[2]) - point.y + line[3])
		/ std::sqrt(m*m + 1);
}

static float wellFitting(const std::vector<cv::Point2f>& v)
{
	cv::Vec4f line;
	cv::fitLine(v, line, cv::DIST_L2, 0, 0.01, 0.01);

	float err = 0;
	for (const auto& i : v)
		err += std::fabs(distPointLine(line, i));
	return err;
}

static int bestTwoLines(const std::vector<cv::Point2f>& v)
{
	float bestErr = std::numeric_limits<float>::max();
	int bestI;

	for (unsigned int i = 2; i < v.size() - 2; i++) {
		float err = wellFitting(std::vector<cv::Point2f>(v.begin(), v.begin() + i)) 
					+ wellFitting(std::vector<cv::Point2f>(v.begin() + i, v.end()));

		if (err < bestErr) {
			bestErr = err;
			bestI = i;
		}
	}

	return bestI;
}

std::tuple<std::map<float, float>, std::map<float, float>, int> projection(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, bool vis)
{
	pcl::visualization::PCLPlotter plotter("plotter"), diffPlotter("Diffs plotter");
	std::vector<std::pair<double, double>> points;

	std::map<float, float> lowPoints, highPoints;
	for (const auto& i : *cloud) {
		points.push_back(std::make_pair(i.x, i.y));
		lowPoints[i.x] = lowPoints.find(i.x) != lowPoints.end() ? std::min(i.y, lowPoints[i.x]) : i.y;
		highPoints[i.x] = highPoints.find(i.x) != highPoints.end() ? std::max(i.y, highPoints[i.x]) : i.y;
	}

	if (vis) {
		plotter.addPlotData(points, "All points", vtkChart::POINTS);
		points.clear();
		std::transform(lowPoints.begin(), lowPoints.end(), std::back_inserter(points), [](const auto& x){ return std::make_pair(x.first, x.second); });
		std::transform(highPoints.begin(), highPoints.end(), std::back_inserter(points), [](const auto& x){ return std::make_pair(x.first, x.second); });
		plotter.addPlotData(points, "Contour points", vtkChart::POINTS);
		plotter.plot();
	}

	std::vector<cv::Point2f> diff;
	for (const auto& i : boost::combine(lowPoints, highPoints)) {
		std::pair<float, float> pa, pb;
		boost::tie(pa, pb) = i;
		diff.push_back(cv::Point2f(pa.first, std::fabs(pa.second - pb.second)));
	}

	if (vis) {
		points.clear();
		std::transform(diff.begin(), diff.end(), std::back_inserter(points), [](const auto& x){ return std::make_pair(x.x, x.y); });
		diffPlotter.addPlotData(points, "Margin diffs", vtkChart::POINTS);
	}

	int split = bestTwoLines(diff);

	if (vis) {
		cv::Vec4f line;
		cv::fitLine(std::vector<cv::Point2f>(diff.begin(), diff.begin() + split), line, cv::DIST_L2, 0, 0.01, 0.01);
		points.clear();
		points.push_back(std::make_pair(diff[0].x, applyPoint(line, diff[0].x).y));
		points.push_back(std::make_pair(diff[split - 1].x, applyPoint(line, diff[split - 1].x).y));
		diffPlotter.addPlotData(points, "Fit line 1");

		cv::fitLine(std::vector<cv::Point2f>(diff.begin() + split, diff.end()), line, cv::DIST_L2, 0, 0.01, 0.01);
		points.clear();
		points.push_back(std::make_pair(diff[split].x, applyPoint(line, diff[split].x).y));
		points.push_back(std::make_pair(diff.back().x, applyPoint(line, diff.back().x).y));
		diffPlotter.addPlotData(points, "Fit line 2");
		diffPlotter.plot();
	}

	return std::make_tuple(lowPoints, highPoints, split);
}
