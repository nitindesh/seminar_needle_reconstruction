#ifndef PROJECTION_H
#define PROJECTION_H

#include <pcl/point_cloud.h>
#include <pcl/common/common_headers.h>
#include <map>

std::tuple<std::map<float, float>, std::map<float, float>, int> projection(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, bool vis = false);

#endif
