#include <segmentation.h>

#include <fstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <string>
#include <opencv2/opencv.hpp>

#include <pcl/common/common_headers.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/features/normal_3d.h>
#include <pcl/common/centroid.h>

#include <thread>
#include <chrono>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

static bool customSort(pcl::PointCloud<pcl::PointXYZ> x, pcl::PointCloud<pcl::PointXYZ> y) {
    return x.size() > y.size();
}

// static bool customSortRGB(pcl::PointCloud<pcl::PointXYZRGB> x, pcl::PointCloud<pcl::PointXYZRGB> y) {
//     return x.size() > y.size();
// }

static std::vector<std::string> getImagePaths(const std::string& path)
{
	boost::regex fname("^\\d{3}\\.bmp$");
	std::vector<std::string> paths;

	for (boost::filesystem::directory_iterator it(path); it != boost::filesystem::directory_iterator(); ++it)
		if (boost::filesystem::is_regular_file(it->status()) && boost::regex_match(it->path().filename().string(), fname))
			paths.push_back(it->path().string());

	std::sort(paths.begin(), paths.end());
	return paths;
}

bool waitKey = false;
int volumeBScans = 128;

static pcl::PointCloud<pcl::PointXYZI>::Ptr point_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>);
static pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcl_visualization (new pcl::PointCloud<pcl::PointXYZRGB>);

// static boost::shared_ptr<pcl::visualization::PCLVisualizer> simpleVis(pcl::PointCloud<pcl::PointXYZI>::ConstPtr cloud, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> rgb)
// {
// 	// --------------------------------------------
// 	// -----Open 3D viewer and add point cloud-----
// 	// --------------------------------------------
// 	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
// 	viewer->setBackgroundColor(0, 0, 0); 
// 	viewer->addPointCloud<pcl::PointXYZI>(cloud, rgb, "sample cloud");
// 	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
// 	viewer->addCoordinateSystem(1.0);
// 	viewer->initCameraParameters();
// 	viewer->spinOnce();
// 	return (viewer);
// }
// static boost::shared_ptr<pcl::visualization::PCLVisualizer> xyzVis(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
// {
//     // --------------------------------------------
//     // -----Open 3D viewer and add point cloud-----
//     // --------------------------------------------
//     boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
//     viewer->setBackgroundColor(0, 0, 0);
//     //pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZ> rgb(cloud);
//     viewer->addPointCloud<pcl::PointXYZ>(cloud, "sample cloud");
//     viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
//     viewer->addCoordinateSystem(1.0);
//     viewer->initCameraParameters();
//     viewer->spinOnce();
//     return (viewer);
// }

static boost::shared_ptr<pcl::visualization::PCLVisualizer> rgbVis (pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud)
{
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return (viewer);
}

void MatToPoinXYZ(cv::Mat& OpencVPointCloud, cv::Mat& labelInfo, int z, pcl::PointCloud<pcl::PointXYZI>::Ptr& point_cloud_ptr, int height, int width)
{
	//get the infos for the bounding box
	int x = labelInfo.at<int>(0, cv::CC_STAT_LEFT);
	int y = labelInfo.at<int>(0, cv::CC_STAT_TOP);
	int labelWidth = labelInfo.at<int>(0, cv::CC_STAT_WIDTH);
	int labelHeight = labelInfo.at<int>(0, cv::CC_STAT_HEIGHT);
	//go through points in bounding box
	for (int j = y; j < y+labelHeight; j++) {
		//indicate if first point with intensity = 1 in row has been found
		bool firstNotFound = true;
		//position of last point with intensity = 1 in row
		int lastPointPosition = 0;
		for (int i = x; i < x+labelWidth; i++)
		{
			if (OpencVPointCloud.at<uchar>(j,i) >= 1.0f){
				if (firstNotFound) {
					firstNotFound = false;
				}
				lastPointPosition = i;
			}
		}
		if (!firstNotFound) {
			//add the last point with intensity = 1 in row to the point cloud
			pcl::PointXYZI point;
			point.x = (float)z / volumeBScans * 2.6f;
			point.y = (float)j / height * 3.0f;
			point.z = (float)lastPointPosition / width * 2.0f;
			point.intensity = OpencVPointCloud.at<uchar>(j, lastPointPosition);
			point_cloud_ptr->points.push_back(point);
		}
	}
}

pcl::PointCloud<pcl::PointXYZRGB>::Ptr segment(std::string inputDirectory)
{
	//get the path to the images
	std::replace(inputDirectory.begin(), inputDirectory.end(), '\\', '/');
	int lastSlashIndex = inputDirectory.find_last_of('/', inputDirectory.size());
	if (lastSlashIndex < (int)inputDirectory.size() - 1)
		inputDirectory += "/";

	int number = 0;
	int countIm = 0;

	//	go through all frames
	for (const auto& path : getImagePaths(inputDirectory))
	{
		//read the image in grayscale
		cv::Mat imageGray = cv::imread(path, CV_LOAD_IMAGE_GRAYSCALE);

		//flip and transpose the image
		cv::Mat transposedOCTimage;
		cv::flip(imageGray, imageGray, 0);
		cv::transpose(imageGray, transposedOCTimage);

		//set a threshold (0.26)
		cv::Mat thresholdedImage;
		cv::threshold(transposedOCTimage, thresholdedImage, 0.26 * 255, 1, 0);

		//use a median blur filter
		cv::Mat filteredImage;
		cv::medianBlur(thresholdedImage, filteredImage, 3);

		//label the image
		cv::Mat labelledImage;
		cv::Mat labelStats;
		cv::Mat labelCentroids;
		int numLabels = cv::connectedComponentsWithStats(thresholdedImage, labelledImage, labelStats, labelCentroids);

		//for every label with more than 400 points process it further for adding points to the cloud
		for (int i = 1; i < numLabels; i++) {
			if (labelStats.at<int>(i, cv::CC_STAT_AREA) > 400) {
				cv::Mat labelInfo = labelStats.row(i);
				MatToPoinXYZ(filteredImage, labelInfo, number, point_cloud_ptr, thresholdedImage.rows, thresholdedImage.cols);
			}
		}
		number++;

		//show the images
		thresholdedImage = thresholdedImage*255;
		filteredImage = filteredImage*255;
		cv::imshow("OCT", filteredImage);
		cv::waitKey(1);
		countIm = countIm + 1;

	}

    // copy point cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    cloud_filtered->points.resize(point_cloud_ptr->size());
    for (size_t i = 0; i < point_cloud_ptr->points.size(); i++) {
        cloud_filtered->points[i].x = point_cloud_ptr->points[i].x;
        cloud_filtered->points[i].y = point_cloud_ptr->points[i].y;
        cloud_filtered->points[i].z = point_cloud_ptr->points[i].z;
    }
    // Compute Normals
    pcl::search::Search<pcl::PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> > (new pcl::search::KdTree<pcl::PointXYZ>);
    pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
    normal_estimator.setSearchMethod (tree);
    normal_estimator.setInputCloud (cloud_filtered);
    normal_estimator.setRadiusSearch(0.1);
    normal_estimator.compute (*normals);

    // Create region-growing segmentation
    pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
    reg.setMinClusterSize (20);
    reg.setMaxClusterSize (1000000);
    reg.setSearchMethod (tree);
    reg.setNumberOfNeighbours (15);
    reg.setInputCloud (cloud_filtered);
    reg.setInputNormals (normals);
    reg.setSmoothnessThreshold (3.0 / 180.0 * M_PI);
    reg.setCurvatureThreshold (5.0);

    std::vector<pcl::PointIndices> cluster_indices;
    reg.extract(cluster_indices);

    std::vector<pcl::PointXYZ> centroids;
    std::vector<pcl::PointCloud<pcl::PointXYZ>> clusters;
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
    {
        pcl::PointCloud<pcl::PointXYZ> cloud_cluster;
        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
            cloud_cluster.points.push_back (cloud_filtered->points[*pit]);
        cloud_cluster.width = cloud_cluster.points.size ();
        cloud_cluster.height = 1;
        cloud_cluster.is_dense = true;
        std::cout << "PointCloud representing the Cluster: " << cloud_cluster.points.size () << " data points." << std::endl;
        clusters.push_back(cloud_cluster);

    }
    std::sort(clusters.begin(), clusters.end(), customSort);
    std::cout << clusters.size() << std::endl;
    for(unsigned int i=0; i<clusters.size(); i++) {
        pcl::CentroidPoint<pcl::PointXYZ> cp;
        for (unsigned int j=0; j<clusters[i].size(); j++) {
            cp.add(clusters[i][j]);
        }
        pcl::PointXYZ centroid;
        cp.get(centroid);
        centroids.push_back(centroid);
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr joint_clusters (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr needle_pcl (new pcl::PointCloud<pcl::PointXYZRGB>);
    std::srand(std::time(0));
    float rgb_needle = 255*255*255;

    cout << "Centroids: " << centroids.size() << std::endl;

        for(unsigned int i =0; i<clusters.size(); i++) {
            if(clusters.size() > 4) {
                if (centroids[i].z > centroids[0].z) {
                    *joint_clusters += clusters[i];
                }
            }
            else {
                *joint_clusters += clusters[i];
            }
        }

    cout << "Joint clusters" << std::endl;

    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (0.1);
    ec.setMinClusterSize (20);
    ec.setMaxClusterSize (25000);
    ec.setSearchMethod (tree);
    ec.setInputCloud (joint_clusters);
    std::vector<pcl::PointIndices> ec_indices;
    ec.extract (ec_indices);

    cout << "Euclidean Cluster Segmentation" << std::endl;

    std::vector<pcl::PointCloud<pcl::PointXYZ>> ecClusters;
    for (std::vector<pcl::PointIndices>::const_iterator it = ec_indices.begin (); it != ec_indices.end (); ++it)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit) {
            pcl::PointXYZ point = joint_clusters->points[*pit];
            cloud_cluster->points.push_back(point); //*
        }
        cloud_cluster->width = cloud_cluster->points.size ();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;
        ecClusters.push_back(*cloud_cluster);
        std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;

    }
    std::sort(ecClusters.begin(), ecClusters.end(), customSort);


        for(unsigned int i=0; i < clusters.size(); i++){
            float rgb = (float) std::rand();
            for(unsigned int j=0; j < clusters[i].size(); j++) {
                pcl::PointXYZRGB point;
                point.x = clusters[i][j].x;
                point.y = clusters[i][j].y;
                point.z = clusters[i][j].z;
                point.rgb = rgb;
                pcl_visualization->push_back(point);
            }
        }

    for(unsigned int i= 0; i<ecClusters.front().size(); i++) {
        pcl::PointXYZRGB point;
        point.x = ecClusters.front()[i].x;
        point.y = ecClusters.front()[i].y;
        point.z = ecClusters.front()[i].z;
        point.rgb = rgb_needle;
        needle_pcl->push_back(point);
    }
    *pcl_visualization += *needle_pcl;

    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr segmentation_pcl (new pcl::PointCloud<pcl::PointXYZRGB>);
    // for(int i =0; i<clusters.size(); i++) {
    //     float rgb = (float)std::rand();
    //     pcl::PointXYZRGB centroid;
    //     centroid.x = centroids[i].x;
    //     centroid.y = centroids[i].y;
    //     centroid.z = centroids[i].z;
    //     centroid.rgb = (float)255*255*255;
    //     segmentation_pcl->push_back(centroid);
    //     for( int j =0; j <clusters[i].size(); j++) {
    //         pcl::PointXYZRGB point;
    //         point.x = clusters[i][j].x;
    //         point.y = clusters[i][j].y;
    //         point.z = clusters[i][j].z;
    //         point.rgb = rgb;
    //         segmentation_pcl->push_back(point);
    //     }
    // }

    return needle_pcl;
}

void segVisualize(pcl::PointCloud<pcl::PointXYZRGB>::Ptr needle_pcl)
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> rgb_viewer = rgbVis(pcl_visualization);
    rgb_viewer->updatePointCloud(pcl_visualization, "sample_cloud");
    boost::shared_ptr<pcl::visualization::PCLVisualizer> needle_viewer = rgbVis(needle_pcl);
    needle_viewer->updatePointCloud(needle_pcl, "needle segmentation");

	while (!rgb_viewer->wasStopped())
	{
		rgb_viewer->spinOnce(100);
		std::this_thread::sleep_for(std::chrono::microseconds(100000));
	}
}
