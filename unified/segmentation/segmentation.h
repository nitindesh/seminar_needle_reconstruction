#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include <pcl/point_cloud.h>
#include <pcl/common/common_headers.h>

pcl::PointCloud<pcl::PointXYZRGB>::Ptr segment(std::string inputDirectory);
void segVisualize(pcl::PointCloud<pcl::PointXYZRGB>::Ptr needle_pcl);

#endif
