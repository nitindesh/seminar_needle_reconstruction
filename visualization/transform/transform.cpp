#include "transform.h"

#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>

TransformablePolygonMesh::TransformablePolygonMesh()
{
	reset();
}

Eigen::Affine3f& TransformablePolygonMesh::transform()
{
	return m_transform;
}

void TransformablePolygonMesh::reset()
{
	m_transform = Eigen::Affine3f::Identity();
}

void TransformablePolygonMesh::apply()
{
	pcl::PointCloud<pcl::PointXYZI> cloud;
	pcl::fromPCLPointCloud2(this->cloud, cloud);
	pcl::transformPointCloud(cloud, cloud, m_transform);
	pcl::toPCLPointCloud2(cloud, this->cloud);
}

