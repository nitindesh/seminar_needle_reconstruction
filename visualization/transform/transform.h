#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <pcl/io/vtk_lib_io.h>

class TransformablePolygonMesh : public pcl::PolygonMesh {
public:
	enum Axis { X, Y, Z };

	TransformablePolygonMesh();

	Eigen::Affine3f& transform();
	void reset();
	void apply();

private:
	// typename pcl::PointCloud<T>::Ptr originalCloud = nullptr;
	Eigen::Affine3f m_transform;
};

#endif
